class Feeler {
  pos = new p5.Vector();
  sensedFood = false;
  sensedBug = false;
  update(size, heading, angle) {
    this.pos.set(0, -size);
    this.pos.rotate(heading + angle);
  }
  senseFood(pos, food) {
    if (collideLineCircle(pos.x, pos.y, pos.x + this.pos.x, pos.y + this.pos.y, food.pos.x, food.pos.y, food.amount)) {
      this.sensedFood = true;
    }
  }
  senseBug(pos, bug) {
    if (collideLineCircle(pos.x, pos.y, pos.x + this.pos.x, pos.y + this.pos.y, bug.pos.x, bug.pos.y, bug.size)) {
      this.sensedBug = true;
    }
  }
  draw(pos) {
    //if (!this.sensedFood) return;
    //stroke(0);
    //line(pos.x, pos.y, pos.x + this.pos.x, pos.y + this.pos.y);
  }
}

class NeuralNet {
  constructor(numInputs, numHidden, numOutputs) {
    this.inputWeights = Array.from({ length: numInputs * numHidden }).map((_) => random(-1, 1));
    this.outputWeights = Array.from({ length: numHidden * numOutputs }).map((_) => random(-1, 1));
    this.hidden = Array.from({ length: numHidden }).fill(0);
    this.outputs = Array.from({ length: numOutputs }).fill(0);
  }
  update(...inputs) {
    for (let j = 0; j < this.hidden.length; j++) {
      this.hidden[j] = 0;
      for (let i = 0; i < inputs.length; i++) {
        this.hidden[j] += inputs[i] * this.inputWeights[j * inputs.length + i];
      }
      this.hidden[j] = constrain(this.hidden[j], -1, 1);
    }
    for (let j = 0; j < this.outputs.length; j++) {
      this.outputs[j] = 0;
      for (let i = 0; i < this.hidden.length; i++) {
        this.outputs[j] += this.hidden[i] * this.outputWeights[j * this.hidden.length + i];
      }
      this.outputs[j] = constrain(this.outputs[j], -1, 1);
    }
  }
  inherit(nn) {
    const shouldMutate = random() < 0.1;
    for (let i = 0; i < this.inputWeights.length; i++) {
      this.inputWeights[i] = nn.inputWeights[i];
      if (shouldMutate) {
        this.inputWeights[i] = constrain(this.inputWeights[i] + random(-0.2, 0.2), -1, 1);
      }
    }
    for (let i = 0; i < this.outputWeights.length; i++) {
      this.outputWeights[i] = nn.outputWeights[i];
      if (shouldMutate) {
        this.outputWeights[i] = constrain(this.outputWeights[i] + random(-0.2, 0.2), -1, 1);
      }
    }
  }
}

function generateName() {
  const vowels = "aeiou";
  const consonants = "bdfghjklmpqrstz";
  return (
    consonants[floor(random(consonants.length))] +
    vowels[floor(random(vowels.length))] +
    consonants[floor(random(consonants.length))]
  );
}

function nameToHue(name) {
  let hue = 0;
  for (const char of name) {
    hue = (hue + char.charCodeAt(0) * 10) % 360;
  }
  console.log(name, hue);
  return hue;
}

class Bug {
  firstName = generateName();
  lastName = generateName();
  color = color(`hsl(${nameToHue(this.lastName)}, 50%, 80%)`);
  pos = new p5.Vector(width / 2, height / 2);
  vel = new p5.Vector(random(-1, 1), random(-1, 1));
  size = 10;
  health = 0;
  feelers = [new Feeler(), new Feeler(), new Feeler(), new Feeler()];
  nn = new NeuralNet(9, 10, 2);
  update() {
    this.nn.update(
      sin(((frameCount % 100) / 100) * 2 * PI),
      this.feelers[0].sensedFood ? 1 : -1,
      this.feelers[1].sensedFood ? 1 : -1,
      this.feelers[2].sensedFood ? 1 : -1,
      this.feelers[3].sensedFood ? 1 : -1,
      this.feelers[0].sensedBug ? 1 : -1,
      this.feelers[1].sensedBug ? 1 : -1,
      this.feelers[2].sensedBug ? 1 : -1,
      this.feelers[3].sensedBug ? 1 : -1
    );
    this.vel.normalize();
    this.vel.mult(1 + this.nn.outputs[0] * 1.9);
    this.vel.rotate(radians(this.nn.outputs[1] * 20));
    this.pos.add(this.vel);

    if (this.pos.x < 0) this.vel.x = abs(this.vel.x);
    if (this.pos.x > width) this.vel.x = -1 * abs(this.vel.x);
    if (this.pos.y < 0) this.vel.y = abs(this.vel.y);
    if (this.pos.y > height) this.vel.y = -1 * abs(this.vel.y);

    const heading = this.vel.heading();
    for (let i = 0; i < this.feelers.length; i++) {
      const feeler = this.feelers[i];
      feeler.update(this.size / 2 + 10, heading, radians(45) + radians(45) * 2 * i);
      feeler.sensedFood = false;
      feeler.sensedBug = false;
    }

    for (const bug of bugs) {
      if (bug === this) continue;
      for (const feeler of this.feelers) {
        feeler.senseBug(this.pos, bug);
        if (feeler.sensedBug) {
          this.health -= 0.0001;
        }
      }
    }

    for (const food of foods) {
      if (food.amount <= 0) continue;
      for (const feeler of this.feelers) {
        feeler.senseFood(this.pos, food);
      }
      if (collideCircleCircle(this.pos.x, this.pos.y, this.size, food.pos.x, food.pos.y, food.amount)) {
        food.amount -= 0.1;
        this.health += 0.1;
      }
    }

    for (const feeler of this.feelers) {
      feeler.draw(this.pos);
    }

    stroke(0);
    fill(lerpColor(color("white"), color("green"), map(this.health, 0, 20, 0, 1)));
    circle(this.pos.x, this.pos.y, this.size);
    //line(this.pos.x, this.pos.y, this.pos.x + this.vel.x * 20, this.pos.y + this.vel.y * 20);
    stroke(this.color);
    strokeWeight(3);
    fill(0);
    text(this.firstName+' ' + this.lastName, this.pos.x, this.pos.y);
    strokeWeight(1);
  }
}

class Food {
  amount = random(5, 15);
  constructor() {
    this.pos = new p5.Vector(0, -random(200, height / 2));
    this.pos.rotate(radians(random(360)));
    this.pos.add(width / 2, height / 2);
  }
  update() {
    if (this.amount <= 0) return;
    stroke(0);
    fill("orange");
    circle(this.pos.x, this.pos.y, this.amount);
  }
}

const bugs = [];
const foods = [];
const numBugs = 50;
const numFoods = 50;
let info;
function setup() {
  const canvas = createCanvas(500, 500);
  textSize(8);
  textAlign(LEFT, TOP);
  for (let i = 0; i < numBugs; i++) {
    bugs.push(new Bug());
  }
  for (let i = 0; i < numFoods; i++) {
    foods.push(new Food());
  }
  const side = createDiv().class("side");
  const newGen = createButton("new gen").parent(side);
  newGen.mousePressed(() => {
    foods.length = 0;
    for (let i = 0; i < numFoods; i++) {
      foods.push(new Food());
    }
    bugs.sort((a, b) => (a.health > b.health ? -1 : 1));
    for (const bug of bugs) {
      bug.health = 0;
      bug.pos.set(width / 2, height / 2);
      bug.vel.set(random(-1, 1), random(-1, 1));
    }
    for (let i = floor(bugs.length * 0.2); i < bugs.length; i++) {
      bugs[i].nn.inherit(bugs[0].nn);
      bugs[i].firstName = generateName();
      bugs[i].lastName = bugs[0].lastName;
      bugs[i].color = color(`hsl(${nameToHue(bugs[i].lastName)}, 50%, 80%)`);
    }
    for (let i = 0; i < floor(bugs.length * 0.2); i++) {
      bugs[i] = new Bug();
    }
  });
  info = createDiv().parent(side).style("white-space", "pre");
}

function draw() {
  background(map(sin(((frameCount % 100) / 100) * 2 * PI), -1, 1, 200, 255));
  strokeWeight(1);
  for (const bug of bugs) {
    bug.update();
  }
  for (const food of foods) {
    food.update();
  }
  strokeWeight(3);
  stroke("white");
  fill("orange");
  bugs.sort((a, b) => (a.health > b.health ? -1 : 1));
  info.html(`
avail. food: ${foods.reduce((a, f) => a + f.amount, 0).toFixed(1)}
best health: ${bugs[0].health.toFixed(1)} ${bugs[0].firstName} ${bugs[0].lastName}
  `);
}
